var express = require('express'),
app = express(),
port = process.env.PORT || 3000,
  
bodyParser = require('body-parser');
  
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.set('views', './api/views');
app.set('view engine', 'pug');
//pp.use('/static', express.static(path.join(__dirname, 'public')))

mongoose = require('mongoose');
Task = require('./api/models/todoModel');

mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/Tododb",{
    //useCreateIndex: true,
    //useNewUrlParser: true,
    useMongoClient: true
});

var routes = require('./api/routes/todoRoutes');
routes(app);

app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'})
});



app.listen(port);

console.log(' Server is running on port: ' + port);
