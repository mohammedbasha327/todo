'use strict';

var mongoose = require('mongoose'),
  Task = mongoose.model('Tasks');



exports.list_all_tasks = function(req, res) {

  Task.count({}, function( err, count){
    if (count != 0){
      var query = Task.find({}).select({ "name" : 1 , "_id": 1, "deadline": 1, "Created_date":1 });
          query.exec(function (err,task) {
            if (err)
              res.send(err);
            var str = task;
            str.forEach(function(item) {
              console.log(item);
              var obj = JSON.stringify(item);
              var data = JSON.parse(obj);
            console.log(data.name);
            
            res.render('viewtaskstemp', { 
              name : data.name,
              createdDate : data.Created_date,
              deadline : data.deadline,
              taskID : data._id,
               })
            }); 
          });
  }
  else{
    res.render('no task');
  }
  });
};

exports.show_create_page = function(req, res) {
    res.render('creation');
};

exports.create_a_task = function(req, res) {
  var proDate = new Date().toLocaleString('en-US', {
    timeZone: 'Asia/Calcutta'
  });



  var new_task = new Task(req.body);

  var bodyDate = new_task.deadline.toLocaleString('en-US', {
    timeZone: 'Asia/Calcutta'
  });
  console.log("Body Date = " + bodyDate);
  console.log("Program Date = " + proDate);

  if (bodyDate < proDate){
    res.send("Date can't be yesterday. The task has not been saved.")
  }
  else{
  new_task.save(function(err, task) {
    if (err)
      res.send(err);
    res.send("<h2>Task added</h2>");
  });
};
};

exports.read_a_task = function(req, res) {
    var query = Task.findById(req.params.taskId).select({ "name" : 1 , "_id": 0, "deadline": 1, "Created_date": 1});
    
    query.exec(function (err,task,next) {
      if (err)
        return next(err);
      var str = task;
        var obj = JSON.stringify(str);
        var data = JSON.parse(obj);
      console.log(data.name);
        if(data.deadline == null){
          console.log("No deadLine");
          var dead = "No DeadLine"
        }else{
          var dead = data.deadline;
        }
      res.render('singletask', { 
        name : data.name,
        createdDate : data.Created_date,
        deadline : dead
      })
      })
          //res.send("Task not Found");
};

exports.update_a_task = function(req, res) {
  Task.findOneAndUpdate({_id:req.params.taskId}, req.body, {new: true}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.delete_a_task = function(req, res) {

  Task.remove({
    _id: req.params.taskId
  }, function(err, task) {
    if (err)
      res.send(err);
    res.json({ message: 'Task successfully deleted' });
  });
};
