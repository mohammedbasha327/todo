'use strict';

module.exports = function(app) {
	var todoList = require('../controllers/todoController');

	// todoList Routes
	app.route('/tasks')
		.get(todoList.list_all_tasks);
	
	//app.get('/create' , (err,res)=> {
	//	res.render('creation');
	//})

	app.route('/create')
		.get(todoList.show_create_page)
		.post(todoList.create_a_task);

	app.route('/tasks/:taskId')
		.get(todoList.read_a_task);

	app.route('/update/:taskId')
		.post(todoList.update_a_task);

	app.route('/delete/:taskId')
		.get(todoList.delete_a_task);
};
