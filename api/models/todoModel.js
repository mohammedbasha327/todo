'use strict';


var mongoose = require('mongoose');
var TaskSchema = new mongoose.Schema({
  name: {
    type: String,
    Required: 'Kindly enter the name of the task'
  },

  Created_date: {
    type: Date,
    default: Date.now
  },

  Priority: {
      type: String,
      enum: ['high', 'normal', 'low'],
      default: 'normal'
    },


  deadline: {
      type: Date,
      default: ''
  }
});


module.exports = mongoose.model('Tasks', TaskSchema);